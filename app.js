const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const cors = require('cors');
const path = require('path');
const TelegramBot = require('node-telegram-bot-api');
const mongoose = require('mongoose');
const botModel = require('./telegram_bot_model');
const botDb = mongoose.model('bot');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use( cors({origin: '*'}) );
app.use(express.static(path.join(__dirname, 'client/dist')));

//connection string
mongoose.connect("mongodb://zencity_bots:bot123@ds145325.mlab.com:45325/datahack", { useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true});

//get tables in db optional method
// mongoose.connection.on('open', function (ref) {
//      console.log('Connected to mongo server.');
//      //trying to get collection names
//      mongoose.connection.db.listCollections().toArray(function (err, names) {
//          console.log("db tables names",  names);
//      });

//      botModel.find({}).then(data=>{console.log("bot data: ************: ", data)}).catch(err => {console.log(err)});
//  })
 
//master bot, has links on other bots
const token = '949608713:AAHaTxGzIeysyGr4JMeahgQe7aDBAKVZaCc';
const botName = 'zencity_service_bot'
const bot = new TelegramBot(token, {polling: true});

bot.on('message', (msg) => {
     console.log("telegram bot obj: ", msg);
     let msgHtml = '';
     botModel.find({})
     .then(data => {

          console.log("master bot data: ", data)
          for(let i=0; i < data.length; i++){
               if(data[i].status !== "engaged"){
                 msgHtml += "\n <a href='http://"+ data[i].contactDetails +"'>Support "+ data[i].expertise +"</a>";
               }               
          }

          console.log("html string: ", msgHtml);
          if(msgHtml === ''){
               msgHtml = 'Sorry, no free bots.'; 
          }
          bot.sendMessage(msg.chat.id, msgHtml, {parse_mode: 'HTML'});
     })
     .catch(err => {
          console.log("get list bots error: ", err);
     })     
});

//customersupbot1 implementation
const bot1 = new TelegramBot('419831156:AAGGe0b1pu7l1nMXKr9FIsMCFN16T9Jy-9g', {polling: true});
bot1.on('message', (msg) => {
     console.log("telegram bot1 obj: ", msg);
     botModel.find({name: "customersupbot1"})
     .then(data => {
          console.log("customersupbot1 data: ", data);
          bot1.sendMessage(msg.chat.id, data[0].answer, {parse_mode: 'HTML'});
          changeStatusBot('customersupbot1');
     })
     .catch(err => {
          console.log("get bot customersupbot1 error: ", err);
     })     
});

//customersupbot2 implementation
const bot2 = new TelegramBot('429977475:AAGJ_nd4kWvLkQrX8Nvb3IQ-wyxOH5GovqY', {polling: true});
bot2.on('message', (msg) => {
     console.log("telegram bot2 obj: ", msg);
     botModel.find({name: "customersupbot2"})
     .then(data => {
          console.log("customersupbot1 data: ", data);
          bot2.sendMessage(msg.chat.id, data[0].answer, {parse_mode: 'HTML'});
          changeStatusBot('customersupbot2');
     })
     .catch(err => {
          console.log("get bot customersupbot1 error: ", err);
     })    
});

//customersupbot3 implementation
const bot3 = new TelegramBot('432976860:AAE_51EwJhuVAVweSRuhEUuQnN2dVcnTvKE', {polling: true});
bot3.on('message', (msg) => {
     console.log("telegram bot3 obj: ", msg);
     botModel.find({name: "customersupbot3"})
     .then(data => {
          console.log("customersupbot1 data: ", data);
          bot3.sendMessage(msg.chat.id, data[0].answer, {parse_mode: 'HTML'});
          changeStatusBot('customersupbot3');
     })
     .catch(err => {
          console.log("get bot customersupbot1 error: ", err);
     })     
});

app.get('/*', (req, res) => {
     console.log("load default page home: ", path.join(__dirname, '/client/dist/index.html'));
     res.sendFile(path.join(__dirname, '/client/dist/index.html'), (err) => {
          if (err) {
              res.status(500).send(err)
          }
      }); 
});

//get list bots
app.post('/get_list_bots', (req, res) => {
     console.log("get bots")
     botModel.find({})
     .then(data => {        
          res.send({success: true, text: '', data: data});
      })
      .catch(err => {
          res.send({success: false, text: 'get_list_bots error: ' + err, data: null});
      })   
});

//reset bots
app.post('/reset_bots', (req, res) => {
     console.log("get bots")
     botModel.updateMany({$set:{status: 'free'}})
     .then(data => {        
          res.send({success: true, text: '', data: data});
      })
      .catch(err => {
          res.send({success: false, text: 'reset_bots error: ' + err, data: null});
      })   
});

//server implementation
const server = app.listen(process.env.PORT || 3003, (err) => {
     console.log("listening on port: 3003...");
});

//socket.io implementation
const io = require('socket.io')(server);

//change bot status in db
function changeStatusBot(nameIn){
     console.log("change status bot for client")
     botModel.update({name: nameIn}, {$set:{status: 'engaged'}})
     .then(data =>
          {console.log("bot data: ************: ", data)
          //alert changes to all connected clients
          io.emit('status_change', true);
     })
     .catch(err => {console.log(err)});     
}
