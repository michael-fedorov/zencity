const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const BotSchema = new Schema({
    name: {type: String, require: true},
    status: {type: String, require: true},
    contactDetails: {type: String, require: true},
    expertise: [{type: String, require: true}],
    answer: {type: String, require: true}
})

const bot = mongoose.model('bot', BotSchema);
module.exports = bot;