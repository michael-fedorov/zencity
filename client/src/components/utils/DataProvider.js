import axios from 'axios';
import config from '../../config';

export function getDataFromServer(url, data){
    var path = config.params.server_path + '/' + url;
    return axios.post(path, { data: data });
}

export function getDataFromServerWithHeader(url, data, header){
  var path = config.params.server_path + '/' + url;
  return axios.post(path, data, {headers: header});
  //return axios.post(savePath, fdata, {headers: { 'Content-Type': 'multipart/form-data' }});
}

