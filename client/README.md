# client-telegram-bot

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Installation
```

[Demo](http://185.241.4.124:3003/)
```

clone project to server
git clone https://michael-fedorov@bitbucket.org/michael-fedorov/zencity.git
```

run npm package installention
npm install
```

configur client config.js
set server ip/hostname in client/config.js
```

run project
node app.js
```
